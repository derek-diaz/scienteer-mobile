import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {IonicApp, IonicModule, IonicErrorHandler} from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpModule } from '@angular/http';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { MessagesServiceProvider } from '../providers/messages-service/messages-service';


//Pages
import { LoginPageModule } from '../pages/login/login.module';
import { MainPageModule } from '../pages/main/main.module';
import { MessagesPageModule } from "../pages/messages/messages.module";
import { ScanPageModule } from "../pages/scan/scan.module";
import {NewMessagePageModule} from "../pages/new-message/new-message.module";
import {ViewMessagePageModule} from "../pages/view-message/view-message.module";

//Pages
import { LoginPage } from '../pages/login/login';
import { MainPage } from '../pages/main/main';
import { MessagesPage } from "../pages/messages/messages";
import {NewMessagePage} from "../pages/new-message/new-message";
import {ScanPage} from "../pages/scan/scan";
import {ViewMessagePage} from "../pages/view-message/view-message";

@NgModule({
  declarations: [
    MyApp,

  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    LoginPageModule,
    MainPageModule,
    MessagesPageModule,
    ViewMessagePageModule,
    NewMessagePageModule,
    ScanPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    MainPage,
    MessagesPage,
    ViewMessagePage,
    NewMessagePage,
    ScanPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthServiceProvider,
    MessagesServiceProvider
  ]
})
export class AppModule {}
