import { Component, ViewChild } from '@angular/core';
import {Platform, Nav, Events, ToastController, LoadingController} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LoginPage } from '../pages/login/login';
import { MainPage } from "../pages/main/main";
import { MessagesPage } from "../pages/messages/messages";
import { ScanPage } from "../pages/scan/scan";
import {AuthServiceProvider} from "../providers/auth-service/auth-service";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  pages: Array<{title: string, component: any}>;

  rootPage:any;
  loading: any;

  userData = { name:'', profilePhoto:'', role:'' };

  dev = "/root/";
  //prod = "http://scienteer.com/api/";
  test = "http://dev.scienteer.com/";
  url = this.dev;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public events: Events,
              public authService: AuthServiceProvider,
              public loadingCtrl: LoadingController, private toastCtrl: ToastController) {
    platform.ready().then(() => {

      //Is user logged in?
      if (localStorage.getItem("token") == "" || localStorage.getItem("token") === null){
        this.nav.setRoot(LoginPage);
      }else{
        this.nav.setRoot(MainPage);
      }

      this.pages = [
        { title: 'Dashboard', component: MainPage },
        { title: 'Messages', component: MessagesPage} ,
        { title: 'Scan Documents', component: ScanPage} ,
        { title: 'Logout', component: LoginPage} ,
      ];

      //Profile Update Event
      events.subscribe('name:changed', name => {
        if(name !== undefined && name !== ""){
          this.userData.name = name;
        }
      });

      events.subscribe('profile:changed', profile => {
        if(profile !== undefined && profile !== ""){
          this.userData.profilePhoto = this.url + profile;
        }
      });

      events.subscribe('role:changed', role => {
        if(role !== undefined && role !== ""){
          if (role == "fairadmin" ) {
            this.userData.role = "Fair Administrator";
          } else if (role == "student"){
            this.userData.role = "Student";
          }else {
            this.userData.role = "-";
          }
        }
      });

      this.userData.profilePhoto =  this.url + localStorage.getItem("profilePhoto");

      if (localStorage.getItem("role") == "fairadmin" ) {
        this.userData.role = "Fair Administrator";
      } else if (localStorage.getItem("role") == "student"){
        this.userData.role = "Student";
      }else {
        this.userData.role = "-";
      }

      this.userData.name = localStorage.getItem("name");



      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  openPage(page) {

    //User is logging out! Wipe local storage.
    if(page.title == "Logout"){
      this.showLoader();
      this.authService.logout().then((result) => {
        this.loading.dismiss();
        this.presentToast("Logged Out Successfully!");
        localStorage.clear();
      }, (err) => {
        localStorage.clear();
        this.loading.dismiss();
        this.presentToast(err);
        console.log(err);

      });
    }
    this.nav.setRoot(page.component);
  }

  showLoader(){
    this.loading = this.loadingCtrl.create({
      content: 'Logging Out...'
    });

    this.loading.present();
  }

  presentToast(msg) {
    this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true
    });
  }

}
