import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

let dev = "/api/";
//let test = "http://dev.scienteer.com/api/";
//let prod = "http://scienteer.com/api/";

let apiUrl = dev;


@Injectable()
export class AuthServiceProvider {

  payload = { email_username:'', password:'' };
  constructor(public http: Http) {}

  login(username, password) {

    //Set Payload
    this.payload.email_username = username;
    this.payload.password  = password;
    let auth= JSON.stringify(this.payload);

    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/jsonp');

      this.http.post(apiUrl+'v1/app/auth/login', auth, {headers: headers})
        .subscribe(res => {
          resolve(res.json());
        }, (err) => {
          reject(err);
        });
    });
  }

  logout() {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/jsonp');
      headers.append('Authorization', "Bearer " + localStorage.getItem("token"));

      this.http.post(apiUrl+'v1/app/auth/logout', "", {headers: headers})
        .subscribe(res => {
          resolve(res.json());
        }, (err) => {
          reject(err);
        });
    });




  }

}
