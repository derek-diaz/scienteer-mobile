import { Component, Pipe} from '@angular/core';
import {DomSanitizer, SafeResourceUrl, SafeUrl, SafeScript, SafeStyle, SafeHtml} from '@angular/platform-browser';
import {IonicPage, NavController, NavParams, ToastController, LoadingController} from 'ionic-angular';
import { MessagesServiceProvider } from '../../providers/messages-service/messages-service';
import {NewMessagePage} from "../new-message/new-message";

/**
 * Generated class for the ViewMessagePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-message',
  templateUrl: 'view-message.html',
})

@Pipe({
  name: 'safe'
})
export class ViewMessagePage {

  public msgId;
  public username;
  public message: any;
  loading: any;
  data: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public msgService: MessagesServiceProvider,
              public loadingCtrl: LoadingController,private toastCtrl: ToastController, protected _sanitizer: DomSanitizer) {
    this.msgId = navParams.get("id");
    this.username = navParams.get("username");
    this.getMessage();
  }

  replyMessage(){
    this.navCtrl.push(NewMessagePage,{username: this.username, subject: this.message.subject});
  };

  getMessage(){
    this.showLoader();
    this.msgService.getMessage(this.msgId).then((result) => {
      this.loading.dismiss();
      this.data = result;
      this.data.message.body = this.transform(this.data.message.body, "html");
      this.message = [this.data.message];

    }, (err) => {
      this.loading.dismiss();
      this.presentToast(err);
      console.log("MESSAGES FAIL: " + err);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewMessagePage');
  }

  showLoader(){
    this.loading = this.loadingCtrl.create({
      content: 'Loading...'
    });

    this.loading.present();
  }

  presentToast(msg) {
    this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true
    });
  }

  public transform(value: string, type: string): SafeHtml | SafeStyle | SafeScript | SafeUrl | SafeResourceUrl {
    switch (type) {
      case 'html':
        return this._sanitizer.bypassSecurityTrustHtml(value);
      case 'style':
        return this._sanitizer.bypassSecurityTrustStyle(value);
      case 'script':
        return this._sanitizer.bypassSecurityTrustScript(value);
      case 'url':
        return this._sanitizer.bypassSecurityTrustUrl(value);
      case 'resourceUrl':
        return this._sanitizer.bypassSecurityTrustResourceUrl(value);
      default:
        throw new Error(`Unable to bypass security for invalid type: ${type}`);
    }
  }

}
