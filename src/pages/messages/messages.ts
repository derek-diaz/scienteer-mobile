import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController,ToastController } from 'ionic-angular';
import { MessagesServiceProvider } from '../../providers/messages-service/messages-service';
import {ViewMessagePage} from "../view-message/view-message";
import {NewMessagePage} from "../new-message/new-message";

@IonicPage()
@Component({
  selector: 'page-messages',
  templateUrl: 'messages.html',
})
export class MessagesPage {

  loading: any;
  data: any;
  public messages: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public msgService: MessagesServiceProvider,
              public loadingCtrl: LoadingController,private toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MessagesPage');
    this.getMessages();
  }

  getMessages(){
    this.showLoader();
    this.msgService.getMessageList().then((result) => {
      this.loading.dismiss();
      this.data = result;
      this.messages = this.data.messages
      console.log(this.data.messages);

    }, (err) => {
      this.loading.dismiss();
      this.presentToast(err);
      console.log("MESSAGES FAIL: " + err);
    });
  }

  doRefresh(event){
    this.showLoader();
    this.msgService.getMessageList().then((result) => {
      this.loading.dismiss();
      this.data = result;
      this.messages = this.data.messages;
      if (this.messages.length == 0){
        this.messages = null;
      }
      console.log(this.data.messages);
      event.complete();

    }, (err) => {
      this.loading.dismiss();
      this.presentToast(err);
      event.complete();
      console.log("MESSAGES FAIL: " + err);
    });

  }

  deleteMessage(id){
    this.showLoader();
    this.msgService.deleteMessage(id).then((result) => {
      this.msgService.getMessageList().then((result) => {
        this.loading.dismiss();
        this.data = result;
        this.messages = this.data.messages;
        console.log(this.data.messages);
        this.presentToast("Message Deleted");
      }, (err) => {
        this.loading.dismiss();
        this.presentToast(err);
        console.log("MESSAGES FAIL: " + err);
      });
    }, (err) => {
      this.loading.dismiss();
      this.presentToast(err);
      console.log("MESSAGES FAIL: " + err);
    });

  }

  replyMessage(id, username, subject){
    this.navCtrl.push(NewMessagePage,{username: username, subject: subject})
  }

  newMessage(){
    this.navCtrl.push(NewMessagePage);
  }

  openMessage(msgId, user){
    this.navCtrl.push(ViewMessagePage,{id: msgId, username: user});
  }

  showLoader(){
    this.loading = this.loadingCtrl.create({
      content: 'Loading...'
    });

    this.loading.present();
  }

  presentToast(msg) {
    this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true
    });
  }
}
