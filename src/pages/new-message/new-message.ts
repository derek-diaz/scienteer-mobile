import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, LoadingController, ToastController} from 'ionic-angular';
import {MessagesServiceProvider} from "../../providers/messages-service/messages-service";

/**
 * Generated class for the NewMessagePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-new-message',
  templateUrl: 'new-message.html',
})
export class NewMessagePage {

  loading: any;
  message = { user:'', subject:'', body:'' };
  public username;
  public subject;

  constructor(public navCtrl: NavController, public navParams: NavParams, public msgService: MessagesServiceProvider,
  public loadingCtrl: LoadingController,private toastCtrl: ToastController) {
    this.username = navParams.get("username");
    this.subject = navParams.get("subject");

    if (this.subject){
      this.message.subject = this.subject;
    }

    if (this.username){
      this.message.user = this.username;
    }
  }

  sendMessage(){
    if (!this.message.body || !this.message.subject || !this.message.user){
      this.presentToast("Missing Information, Check message.");
    }else {

      this.showLoader();
      this.msgService.sendMessage(this.message.user, this.message.subject, this.message.body).then((result) => {
        this.loading.dismiss();

        this.presentToast("Message Sent Successfully");
        this.navCtrl.pop();

      }, (err) => {
        this.loading.dismiss();
        this.presentToast(err);
      });
    }

  }

  showLoader(){
    this.loading = this.loadingCtrl.create({
      content: 'Sending...'
    });

    this.loading.present();
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }



}
