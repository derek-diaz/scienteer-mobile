import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, MenuController, Events } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { MainPage } from '../main/main';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  loading: any;
  loginData = { username:'', password:'' };
  data: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public authService: AuthServiceProvider,
              public loadingCtrl: LoadingController, private toastCtrl: ToastController, public menu: MenuController,
              public events: Events) {
    //Disable Menu for Login Screen
    this.menu.enable(false, 'menu');

  }

  doLogin(){
    this.showLoader();
    this.authService.login(this.loginData.username, this.loginData.password ).then((result) => {
      this.loading.dismiss();
      this.data = result;

      localStorage.setItem("token", this.data.token);
      localStorage.setItem("profilePhoto", this.data.profilePhoto);
      localStorage.setItem("isPremium", this.data.premium);
      localStorage.setItem("name", this.data.name);
      localStorage.setItem("role", this.data.userTypes[Object.keys(this.data.userTypes)[0]]);

      console.log("New Token: " + localStorage.getItem("token"));
      console.log("New Profile: " + localStorage.getItem("profilePhoto"));
      console.log("New Premium: " + localStorage.getItem("isPremium"));
      console.log("New Role: " + localStorage.getItem("role"));
      console.log("New Name: " + localStorage.getItem("name"));

      //Update Side Menu
      this.events.publish('profile:changed', localStorage.getItem("profilePhoto"));
      this.events.publish('name:changed', localStorage.getItem("name"));
      this.events.publish('role:changed', localStorage.getItem("role"));

      this.navCtrl.setRoot(MainPage);

    }, (err) => {
      this.loading.dismiss();
      this.presentToast(err);
    });
  }

  showLoader(){
    this.loading = this.loadingCtrl.create({
      content: 'Authenticating...'
    });

    this.loading.present();
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }


}
